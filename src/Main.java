import java.util.Arrays;

import models.Discount;
import models.DiscountCondition;
import models.DiscountResult;
import models.Item;
import models.ShoppingBasket;

public class Main {
	public static void main(String[] args) {
		Item butter = new Item("Butter", 0.80);
		Item milk = new Item("Milk", 1.15);
		Item bread = new Item("Bread", 1);
		
		
		Discount discount1 = new Discount(
	    Arrays.asList(
				new DiscountCondition(butter, 2), 
				new DiscountCondition(bread, 1)
			),
			Arrays.asList(
				new DiscountResult(bread, 1, 0.5)
			)
		); 
		
		
		Discount discount2 = new Discount(
			Arrays.asList(
				new DiscountCondition(milk, 4)
			),
			Arrays.asList(
				new DiscountResult(milk, 1, 1)
			)
		);
		
		ShoppingBasket shoppingBasket = new ShoppingBasket();
		shoppingBasket.addItem(milk, 8);
		shoppingBasket.addItem(butter, 2);
		shoppingBasket.addItem(bread, 1);
	
		
		shoppingBasket.getTotalPrice(Arrays.asList(discount1, discount2));
	}
}
