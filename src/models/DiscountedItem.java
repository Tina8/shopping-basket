package models;

public class DiscountedItem implements IBasketItem {
	private IBasketItem addedItem;
	private DiscountResult discountResult;
	private int applyCount;
	
	
	public DiscountedItem(IBasketItem addedItem, DiscountResult discountResult, int applyCount) {
		this.addedItem = addedItem;
		this.applyCount = applyCount;
		this.discountResult = discountResult;
	}
	
	
	@Override
	public double getTotalPrice() {
		return addedItem.getTotalPrice() - discountResult.getTotalPriceCorrection() * applyCount;
	}

  @Override
  public Item getItem() {
    return addedItem.getItem();
  }

  @Override
  public double getQuantity() {
    return addedItem.getQuantity();
  }
  
  @Override
  public String toString() {
    String originalItem = String.format("%s", addedItem.toString());
    String discount = String.format("%-42s %-13s  %-8d -%-12.2f %.2f", 
        "", discountResult.toString(), applyCount, discountResult.getTotalPriceCorrection()* applyCount, getTotalPrice());
    return originalItem + "\n" + discount;
  }
}
