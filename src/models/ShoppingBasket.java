package models;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShoppingBasket {
	private List<IBasketItem> addedItems = new ArrayList<>();
	public static final String EQUALS = new String(new char[90]).replace("\0", "=");
	
	
	public void addItem(Item item, double quantity) {
		this.addedItems.add(new AddedItem(item, quantity));
	}

	
	private List<IBasketItem> applyDiscounts(List<Discount> discounts) {
	  List<IBasketItem> resultList = new ArrayList<>();
	  
	  boolean first = true;
	  for (Discount discount : discounts) {
      if (first) {
        resultList = addedItems;
        first = false;
      }
      
      resultList = discount.apply(resultList);
    }
	  
	  return resultList;
	}

	
	public double getTotalPrice(List<Discount> discounts) {
	  List<IBasketItem> resultList = applyDiscounts(discounts);
	  
	  String header = String.format("%-15s %-12s %-12s %-15s %-22s %-15s", 
	                "Item", "Unit price", "Quantity", "Discount/#unit", "#","Total");
	  System.out.println(header);
	  System.out.println(EQUALS);
	  
	  double totalPrice = 0;
	  for (IBasketItem basketItem : resultList) {
      System.out.println(basketItem.toString());
      totalPrice += basketItem.getTotalPrice();
    }
	  
	  System.out.println(EQUALS);
	  System.out.println(String.format("%85.2f", totalPrice));
	  
	  return totalPrice;
	}
}