package models;

public interface IBasketItem {

	public double getTotalPrice();
	public Item getItem();
	public double getQuantity();
}
