package models;

import java.util.ArrayList;
import java.util.List;

public class DiscountResult {
	private Item item;
	private double count;
	private double priceCorrection;
	
	
	public DiscountResult(Item item, double count, double priceCorrection) {
		this.item = item;
		this.count = count;
		this.priceCorrection = priceCorrection;
	}


	public double getTotalPriceCorrection() {
		return (item.getPrice() * count) * priceCorrection;
	}
	
	public List<IBasketItem> apply(List<IBasketItem> basketItems, int applyCount) {
	  List<IBasketItem> resultList = new ArrayList<>();
	  
	  for (IBasketItem basketItem : basketItems) {
	    if (basketItem.getItem() != item ) {
	      resultList.add(basketItem);
	      continue;
	    }
	    
	    resultList.add(new DiscountedItem(basketItem, this, applyCount));
    }
	  
	  return resultList;
	}
	
	@Override
	public String toString() {
	  return String.format("%.0f%%/%.2f", priceCorrection * 100, count);
	}
}
