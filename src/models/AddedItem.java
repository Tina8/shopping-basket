package models;

public class AddedItem implements IBasketItem {
	private Item item;
	private double quantity;
	
	public static final String DASHES = new String(new char[90]).replace("\0", "-");

	public AddedItem(Item item, double quantity) {
		this.item = item;
		this.quantity = quantity;
	}
	
	@Override
	public Item getItem() {
		return item;
	}
	
	@Override
	public double getQuantity() {
		return quantity;
	}
	
	@Override
	public double getTotalPrice() {
		return item.getPrice() * quantity;
	}
	
	@Override
	public String toString() {;
	  return DASHES + "\n" + String.format("%s %-15.2f %40.2f", item.toString(), quantity, getTotalPrice());
	}
}
