package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Discount {
	private List<DiscountCondition> discountConditions;
	private List<DiscountResult> discountResults;
	
	
	public Discount(List<DiscountCondition> discountConditions, List<DiscountResult> discountResults) {
		this.discountConditions = discountConditions;
		this.discountResults = discountResults;
	}
	
	public List<DiscountResult> getDiscountResults() {
		return discountResults;
	}
	
	public int getApplyCount(List<IBasketItem> basketItems) {
	  List<Integer> applyCounts = new ArrayList<>();
	  
	  for (DiscountCondition discountCondition : discountConditions) {
	    Integer applyCount = discountCondition.appliesTo(basketItems);
	    applyCounts.add(applyCount);
	    
      if (applyCount == 0) {
        break;
      }
    }
	  
	  return Collections.min(applyCounts);
	}

  public List<IBasketItem> apply(List<IBasketItem> basketItems) {
    int applyCount = getApplyCount(basketItems);
    
    if (applyCount == 0) {
      return basketItems;
    }
    
    List<IBasketItem> resultList = new ArrayList<>();
    
    boolean firstResult= true;
    for (DiscountResult discountResult : discountResults) {
      if (firstResult) {
        resultList = discountResult.apply(basketItems, applyCount);
        firstResult = false;
        continue;
      }
      
      resultList = discountResult.apply(resultList, applyCount);
    }
    
    return resultList;
  }
}
