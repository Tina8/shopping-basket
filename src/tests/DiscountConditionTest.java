package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import models.AddedItem;
import models.DiscountCondition;
import models.IBasketItem;
import models.Item;

class DiscountConditionTest {

  Item butter = new Item("Butter", 0.80);
  Item milk = new Item("Milk", 1.15);
  Item bread = new Item("Bread", 1);
  
  IBasketItem addedButter = new AddedItem(butter, 0.80);
  IBasketItem addedMilk = new AddedItem(milk, 1.15);
  IBasketItem addedBread = new AddedItem(bread, 1); 
  
  List<IBasketItem> basket1 = Arrays.asList(
      new AddedItem(butter, 0.80),
      new AddedItem(milk, 1.15),
      new AddedItem(bread, 1)
      );
  
  List<IBasketItem> basket2 = Arrays.asList(
      new AddedItem(milk, 4)
      );
  
  @Test
  void conditionDoesntApplieToItems() {
    DiscountCondition condition = new DiscountCondition(milk, 4); 
    assertEquals(0, condition.appliesTo(basket1));
  }
  
  void conditionAppliesToItemsOnce() {
    DiscountCondition condition = new DiscountCondition(milk, 4); 
    assertEquals(1, condition.appliesTo(basket2));
  }
  
  void conditionAppliesToItemsThreeTimes() {
    DiscountCondition condition = new DiscountCondition(milk, 13); 
    assertEquals(3, condition.appliesTo(basket2));
  }

}
